package com.itau.projetocarreira.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.LocalDate;
import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.itau.projetocarreira.models.Endereco;
import com.itau.projetocarreira.models.EnumCargo;
import com.itau.projetocarreira.models.EnumNivelFormacao;
import com.itau.projetocarreira.models.Formacao;
import com.itau.projetocarreira.models.Instituicao;
import com.itau.projetocarreira.models.Interesse;
import com.itau.projetocarreira.models.Objetivo;
import com.itau.projetocarreira.models.Profissao;
import com.itau.projetocarreira.models.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class UsuarioControllerTest {

	@Autowired
	UsuarioController controller;
	
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void retornaUsuarioCadastrado() throws Exception{
        String url = String.format("http://localhost:%s/usuarios", port);
        ResponseEntity<Usuario> resposta = restTemplate.postForEntity(url, usuarioTeste(), Usuario.class);
        
        assertNotNull(resposta.getBody());
    }
    
    public Usuario usuarioTeste() {
    	
    	Endereco endereco = new Endereco();
		//endereco.setId(1);
		endereco.setCep("03105000");
		endereco.setLogradouro("Avenida do Estado");
		endereco.setNumero(5533);
		endereco.setComplemento("Fundos");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setPais("Brasil");
		
		Instituicao instituicao = new Instituicao();
		//instituicao.setId(1);
		instituicao.setNome("Itau");
		instituicao.setEnderecos(new ArrayList<Endereco>());
		instituicao.getEnderecos().add(endereco);
		instituicao.setEndereco(endereco, 0);
    	
		Formacao form = new Formacao();
		//form.setId(1);
		form.setCurso("Matematica");
		form.setNivel(EnumNivelFormacao.BACHARELADO);
		form.setDuracao(48);
		form.setDataConclusao(LocalDate.of(2017, 12, 01));
		form.setCompleto(true);
		form.setInstituicao(instituicao);
		
		Profissao profissao = new Profissao();
		//profissao.setId(1);
		profissao.setFuncao("Administrador");
		profissao.setCargo(EnumCargo.JUNIOR);
		profissao.setSalario(3000);
		profissao.setInicio(LocalDate.of(2016,1,1));
		profissao.setFim(LocalDate.of(2018,1,1));
		profissao.setEmpresa(instituicao);
		
		Interesse hobbie = new Interesse();
		//interesse.setId(1);
		hobbie.setDescricao("Futebol");
		hobbie.setTipo("hobbies");
		Interesse idioma = new Interesse();
		//idioma.setId(1);
		idioma.setDescricao("Ingles");
		idioma.setTipo("idioma");
		Interesse habito = new Interesse();
		//habito.setId(1);
		habito.setDescricao("Acordar cedo");
		habito.setTipo("habitos");
		
		Objetivo objetivo = new Objetivo();
		//objetivo.setId(1);
		objetivo.setDescricao("Tentar dominar o mundo");
		objetivo.setPrazo(24);
		
    	Usuario user = new Usuario();
		//user.setId(1);
		user.setNome("João Neves");
		user.setUsername("jonsnow");
		user.setSenha("inverno123");
		user.setEmail("joao@inverno.com");
		user.setNascimento(LocalDate.of(1989, 01, 01));
		user.setNivel(EnumNivelFormacao.BACHARELADO);
		
		user.setFormacoes(new ArrayList<Formacao>());
		user.getFormacoes().add(form);
		user.setProfissoes(new ArrayList<Profissao>());
		user.getProfissoes().add(profissao);
		
		user.setIdiomas(new ArrayList<Interesse>());
		user.getIdiomas().add(idioma);
		user.setHobbies(new ArrayList<Interesse>());
		user.getHobbies().add(hobbie);
		user.setHabitos(new ArrayList<Interesse>());
		user.getHabitos().add(habito);
		
		user.setObjetivos(new ArrayList<Objetivo>());
		user.getObjetivos().add(objetivo);
		
		return user;
    }
}
