package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class EnderecoTest {
	
	@Test
	public void enderecoUm() {
		Endereco endereco = new Endereco();
		endereco.setId(1);
		endereco.setCep("03105000");
		endereco.setLogradouro("Avenida do Estado");
		endereco.setNumero(5533);
		endereco.setComplemento("Fundos");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setPais("Brasil");
		
		assertEquals(1, endereco.getId());
		assertEquals("03105000", endereco.getCep());
		assertEquals("Avenida do Estado", endereco.getLogradouro());
		assertEquals(5533, endereco.getNumero());
		assertEquals("Fundos", endereco.getComplemento());
		assertEquals("São Paulo", endereco.getCidade());
		assertEquals("SP", endereco.getEstado());
		assertEquals("Brasil", endereco.getPais());
	}
}
