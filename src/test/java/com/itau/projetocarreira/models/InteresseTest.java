package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class InteresseTest {
	
	@Test
	public void interesseUm() {
		Interesse interesse = new Interesse();
		interesse.setId(1);
		interesse.setDescricao("Futebol");
		interesse.setTipo("hobbies");
		
		assertEquals(1, interesse.getId());
		assertEquals("Futebol", interesse.getDescricao());
		assertEquals("hobbies", interesse.getTipo());
	}

}
