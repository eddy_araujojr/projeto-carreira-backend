package com.itau.projetocarreira.models;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class InstituicaoTest {
	
	@Test
	public void instituicaoUm() {
		Instituicao instituicao = new Instituicao();
		instituicao.setId(1);
		instituicao.setNome("Itau");
		Endereco endereco = new Endereco();
		endereco.setId(1);
		endereco.setCep("03105000");
		endereco.setLogradouro("Avenida do Estado");
		endereco.setNumero(5533);
		endereco.setComplemento("Fundos");
		endereco.setCidade("São Paulo");
		endereco.setEstado("SP");
		endereco.setPais("Brasil");
		instituicao.setEnderecos(new ArrayList<Endereco>());
		instituicao.getEnderecos().add(endereco);
		instituicao.setEndereco(endereco, 0);
		List<Endereco> enderecos = instituicao.getEnderecos();
		
		assertEquals(1, instituicao.getId());
		assertEquals("Itau", instituicao.getNome());
		assertEquals(enderecos, instituicao.getEnderecos());
	}
}
