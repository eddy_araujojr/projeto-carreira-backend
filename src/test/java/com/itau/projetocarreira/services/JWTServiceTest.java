package com.itau.projetocarreira.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class JWTServiceTest {
	
	@Test
	public void vefificarTokenTestSucesso() {
		String username = "João Neves";
		JWTService jwt = new JWTService();
		String tokien = jwt.gerarToken(username);
		assertEquals(username, jwt.validarToken(tokien));
	}
	
	@Test
	public void vefificarTokenTestErro() {
		String username1 = "João Neves";
		String username2 = "Dani Targaria";
		JWTService jwt = new JWTService();
		String tokien1 = jwt.gerarToken(username1);
		String tokien2 = jwt.gerarToken(username2);
		assertNotEquals(username1, jwt.validarToken(tokien2));
		assertNotEquals(username2, jwt.validarToken(tokien1));
	}

}
