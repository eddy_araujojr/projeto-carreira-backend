package com.itau.projetocarreira.services;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class PaswordServiceTest {

	@Test
	public void verificaSenhaTest() {
		PasswordService passwords = new PasswordService();
		String senha = "inverno123";
		String senhaHash = passwords.gerarHash(senha);
		assertTrue(passwords.verificarHash(senha, senhaHash));
	}
}
