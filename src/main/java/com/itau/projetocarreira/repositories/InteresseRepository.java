package com.itau.projetocarreira.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.projetocarreira.models.Interesse;

public interface InteresseRepository extends CrudRepository<Interesse, Long>{
	
	public Optional<List<Interesse>> findByDescricao(String descricao);
}
