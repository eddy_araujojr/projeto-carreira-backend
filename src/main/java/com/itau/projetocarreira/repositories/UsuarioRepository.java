package com.itau.projetocarreira.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.itau.projetocarreira.models.Usuario;

@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	
	public Optional<Usuario> findByUsername(String username);

	@Query(value="SELECT u FROM Usuario u INNER JOIN u.profissoes up WHERE up.funcao = :profissaoFuncao")
	public Optional<List<Usuario>> findByProfissoes(@Param("profissaoFuncao") String profissaoFuncao);
	
	@Query(value="SELECT u FROM Usuario u INNER JOIN u.formacoes uf WHERE uf.curso = :formacaoCurso")
	public Optional<List<Usuario>> findByFormacoes(@Param("formacaoCurso") String formacaoCurso);
	
	@Query(value="SELECT u FROM Usuario u INNER JOIN u.idiomas ui WHERE ui.descricao = :idiomaDescricao and ui.tipo = 'idioma'")
	public Optional<List<Usuario>> findByIdiomas(@Param("idiomaDescricao") String idiomaDescricao);
	
	@Query(value="SELECT u FROM Usuario u INNER JOIN u.hobbies uh WHERE uh.descricao = :hobbieDescricao and uh.tipo = 'hobbie'")
	public Optional<List<Usuario>> findByHobbies(@Param("hobbieDescricao") String hobbieDescricao);
	
	@Query(value="SELECT u FROM Usuario u INNER JOIN u.habitos uh WHERE uh.descricao = :habitoDescricao and uh.tipo = 'habitos'")
	public Optional<List<Usuario>> findByHabitos(@Param("habitoDescricao") String habitoDescricao);
	
	@Query(value="SELECT u FROM Usuario u INNER JOIN u.objetivos uo WHERE uo.descricao = :objetivoDescricao")
	public Optional<List<Usuario>> findByObjetivos(@Param("objetivoDescricao") String objetivoDescricao);
	
}

