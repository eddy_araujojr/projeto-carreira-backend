package com.itau.projetocarreira.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import com.itau.projetocarreira.models.Instituicao;
import com.itau.projetocarreira.models.Usuario;
import com.itau.projetocarreira.repositories.EnderecoRepository;
import com.itau.projetocarreira.repositories.InstituicaoRepository;
import com.itau.projetocarreira.repositories.UsuarioRepository;
import com.itau.projetocarreira.services.PasswordService;	

@RestController
@RequestMapping("/usuarios")
@CrossOrigin
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	EnderecoRepository enderecoRepository;
	
	@Autowired
	InstituicaoRepository instituicaoRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscarUsuarios() {
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/{id}")
	public ResponseEntity<?> buscarUsuarios(@PathVariable long id) {
		Optional<Usuario> optionalUsuario = usuarioRepository.findById(id);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Usuario criarUsuario(@RequestBody Usuario usuario) {
		
//		List<Formacao> formacoes;
//		Instituicao instituicao;
//		List<Endereco> enderecos;
//		Optional<Endereco> optionalEndereco;
		
		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);
		
		for (int i = 0; i < usuario.getFormacoes().size(); i++) {
			
			Instituicao formacaoInstituicao = usuario.getFormacoes().get(i).getInstituicao();
			
			if(formacaoInstituicao.getId() == 0) {
				formacaoInstituicao = instituicaoRepository.save(formacaoInstituicao);
				usuario.getFormacoes().get(i).setInstituicao(formacaoInstituicao);
			}
		}
		
		for (int i = 0; i < usuario.getProfissoes().size(); i++) {
			
			Instituicao profissaoInstituicao = usuario.getProfissoes().get(i).getEmpresa();
			
			if(profissaoInstituicao.getId() == 0) {
				profissaoInstituicao = instituicaoRepository.save(profissaoInstituicao);
				usuario.getFormacoes().get(i).setInstituicao(profissaoInstituicao);
			}
		}
		
//		for (int i = 0; i < formacoes.size(); i++) {
//			instituicao = (formacoes.get(i)).getInstituicao();
//			enderecos = instituicao.getEnderecos();
//			for (int j = 0; j < enderecos.size(); j++) {
//				optionalEndereco = enderecoRepository.findByLogradouro((enderecos.get(j)).getLogradouro());
//				if(optionalEndereco.isPresent()) {
//					usuario.getFormacoes().get(i).getInstituicao().setEndereco(optionalEndereco.get(), j);
//				}
//			}
//		}
		
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public Usuario atualizarUsuario(@RequestBody Usuario usuario) {
		if((!usuario.getSenha().equals(null)) && (!usuario.getSenha().equals(""))) {
			String hash = passwordService.gerarHash(usuario.getSenha());
			usuario.setSenha(hash);
		}
		return usuarioRepository.save(usuario);
	}

	//Profissão
	@RequestMapping(method=RequestMethod.GET, path="profissao/{funcao}")
	public ResponseEntity<?>  buscarUsuarioProfissoes(@PathVariable String funcao) {
		
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByProfissoes(funcao);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	//Formação
	@RequestMapping(method=RequestMethod.GET, path="formacao/{curso}")
	public ResponseEntity<?> buscarUsuarioFormacoes(@PathVariable String curso){
		
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByFormacoes(curso);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	//Idioma
	@RequestMapping(method=RequestMethod.GET, path="idioma/{descricao}")
	public ResponseEntity<?> buscarUsuarioIdiomas(@PathVariable String descricao){
		
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByIdiomas(descricao);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	//Hobbie
	@RequestMapping(method=RequestMethod.GET, path="hobbie/{descricao}")
	public ResponseEntity<?> buscarUsuarioHobbie(@PathVariable String descricao){
		
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByHobbies(descricao);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	//Habitos
	@RequestMapping(method=RequestMethod.GET, path="habito/{descricao}")
	public ResponseEntity<?> buscarUsuarioHabito(@PathVariable String descricao){
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByHabitos(descricao);
		if(optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}

	//Objetivo
	@RequestMapping(method=RequestMethod.GET, path="objetivo/{descricao}")
	public ResponseEntity<?> buscarUsuarioObjetivo(@PathVariable String descricao){
		Optional<List<Usuario>> optionalUsuario = usuarioRepository.findByObjetivos(descricao);
		if(!optionalUsuario.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(optionalUsuario.get());
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public Usuario removerUsuario(@RequestBody Usuario usuario) {
		usuarioRepository.delete(usuario);
		return usuario;
	}
}
