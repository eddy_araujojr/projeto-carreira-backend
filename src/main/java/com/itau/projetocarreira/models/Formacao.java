package com.itau.projetocarreira.models;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Formacao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private EnumNivelFormacao nivel;
	
	@ManyToOne//(cascade=CascadeType.ALL)
	private Instituicao instituicao;
	
	@NotNull
	private String curso;

	private boolean isCompleto;
	
	@NotNull
	private int duracao;
	
	private LocalDate dataConclusao;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public EnumNivelFormacao getNivel() {
		return nivel;
	}

	public void setNivel(EnumNivelFormacao nivel) {
		this.nivel = nivel;
	}

	public Instituicao getInstituicao() {
		return instituicao;
	}

	public void setInstituicao(Instituicao instituicao) {
		this.instituicao = instituicao;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public boolean isCompleto() {
		return isCompleto;
	}

	public void setCompleto(boolean isCompleto) {
		this.isCompleto = isCompleto;
	}

	public int getDuracao() {
		return duracao;
	}

	public void setDuracao(int duracao) {
		this.duracao = duracao;
	}

	public LocalDate getDataConclusao() {
		return dataConclusao;
	}

	public void setDataConclusao(LocalDate dataConclusao) {
		this.dataConclusao = dataConclusao;
	}
}
