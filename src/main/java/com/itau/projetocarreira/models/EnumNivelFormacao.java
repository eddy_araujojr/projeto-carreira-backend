package com.itau.projetocarreira.models;

public enum EnumNivelFormacao {
	
	MEDIO("Ensino Médio"),
	FUNDAMENTAL("Ensino Fundamental"),
	GRADUACAO("Graduação"),
	BACHARELADO("Bacharelado"),
	LICENCIATURA("Licenciatura"),
	POSGRADUACAO("Pós-Graduação"),
	MESTRADO("Mestrado"),
	DOUTORADO("Doutorado"),
	PHD("Phd"),
	ESPECIALIZACAO("Especialização");
	
	
	
	public String valorNivelFormacao;
	private EnumNivelFormacao(String valor) {
		valorNivelFormacao = valor;
	}
}
