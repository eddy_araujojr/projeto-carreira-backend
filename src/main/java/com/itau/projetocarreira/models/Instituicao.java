package com.itau.projetocarreira.models;

import java.util.List;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Instituicao {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotNull
	private String nome;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Endereco> enderecos;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}
	
	public void setEndereco(Endereco endereco, int index) {
		this.enderecos.get(index).setId(endereco.getId());
		this.enderecos.get(index).setLogradouro(endereco.getLogradouro());
		this.enderecos.get(index).setNumero(endereco.getNumero());
		this.enderecos.get(index).setComplemento(endereco.getComplemento());
		this.enderecos.get(index).setCidade(endereco.getCidade());
		this.enderecos.get(index).setEstado(endereco.getEstado());
		this.enderecos.get(index).setPais(endereco.getPais());	
	}

}
