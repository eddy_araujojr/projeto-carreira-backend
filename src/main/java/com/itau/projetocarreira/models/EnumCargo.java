package com.itau.projetocarreira.models;

public enum EnumCargo {
	
	PRESIDENTE("Presidente"),
	VICEPRESIDENTE("Vice Presidente"),
	DIRETOR("Diretor"),
	SUPERINTENDENTE("Superintendente"),
	GERENTE("Gerente"),
	COORDENADOR("Coordenador"),
	SUPERVISOR("Supervisor"),
	ENCARREGADO("Encarregado"),
	LIDER("Líder"),
	CONSULTOR("Consultor"),
	ESPECIALISTA("Especialista"),
	COACH("Coach"),
	SENIOR("Sênior"),
	PLENO("Pleno"),
	JUNIOR("Junior"),
	TRAINEE("Trainee"),
	TECNICO("Técnico"),
	ASSISTENTE("Assistente"),
	ESTAGIARIO("Estagiário"),
	APRENDIZ("Aprendiz");
	

	public String valorCargo;
	private EnumCargo(String valor) {
		valorCargo = valor;
	}
	
}
