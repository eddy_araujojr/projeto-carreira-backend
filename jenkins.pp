package {'maven':
   ensure => latest,
}

#package {'mariadb-server':
#   ensure => latest,
# }

#exec {'update mysql':
#     command => '/usr/bin/dnf update -y',
#}

#exec{'start mysql':
#        command => '/usr/bin/systemctl start mariadb'
#} 

#exec{'create password mysql':
#	command => '/usr/bin/mysql_secure_installation'
#} 
#exec {'open mysql-mariadb':
#     command => '/usr/bin/mysql',
#}

class mysql::server {
  
  package { "mariadb-server": ensure => installed }
  package { "mariadb": ensure => installed }

  service { "mysqld":
    enable => true,
    ensure => running,
    require => Package["mariadb-server"],
  }

  file { "/var/lib/mysql/my.cnf":
    owner => "mysql", group => "mysql",
    source => "puppet:///mysql/my.cnf",
    notify => Service["mysqld"],
    require => Package["mysql-server"],
  }
 
  file { "/etc/my.cnf":
    require => File["/var/lib/mysql/my.cnf"],
    ensure => "/var/lib/mysql/my.cnf",
  }

  exec { "set-mysql-password":
    unless => "mysqladmin -uroot -p$mysql_password status",
    path => ["/bin", "/usr/bin"],
    command => "mysqladmin -uroot password $mysql_password",
    require => Service["mysqld"],
  }
}
class myapp::db {
    mysqldb { "projetocarreira":
        user => "root",
        password => "$mysql_password",
    }
}


#exec {'create database':
#     command => '/usr/bin/create database projetocarreira',
#}

exec {'project install':
     command => '/usr/bin/nohup /usr/bin/java -jar Projeto-Carreira.jar > /var/log/nohup/projetoCarreira.log &',
}
